from __future__ import unicode_literals
import frappe
import tweepy

settings = frappe.get_doc("Twitter API Settings")

def get_api_instance():
	auth = tweepy.OAuthHandler(settings.consumer_key, settings.consumer_secret_key)
	auth.set_access_token(settings.access_token_key, settings.access_token_secret)

	return tweepy.API(auth)

# User Authentcation

@frappe.whitelist()
def get_authentication_url():
	settings = frappe.get_doc("Twitter API Settings")
	print(settings.consumer_key, settings.consumer_secret_key)
	auth = tweepy.OAuthHandler(settings.consumer_key, settings.consumer_secret_key)
	redirect_url = auth.get_authorization_url()
	frappe.cache().set_value('request_token', auth.request_token['oauth_token'])
	return redirect_url


@frappe.whitelist()
def authorize(verifier):
	settings = frappe.get_doc("Twitter API Settings")
	auth = tweepy.OAuthHandler(settings.consumer_key, settings.consumer_secret_key)
	token = frappe.cache().get_value('request_token')
	frappe.cache().delete_value('request_token')
	auth.request_token = { 'oauth_token' : token, 'oauth_token_secret' : verifier }
	auth.get_access_token(verifier)
	print(auth.access_token, auth.access_token_secret)
	if auth.access_token:
		frappe.get_doc(dict(doctype="OAuth Token", user=frappe.session.user, token=auth.access_token, token_secret=auth.access_token_secret
)).insert()
		return True
	else:
		return False

def get_user_api(user=None):
	if not user:
		user = frappe.session.user
	try:
		tokens = frappe.get_doc('OAuth Token', user)
		auth = tweepy.OAuthHandler(settings.consumer_key, settings.consumer_secret_key)
		auth.set_access_token(tokens.token, tokens.token_secret)
		api = tweepy.API(auth)
	except frappe.DoesNotExistError:
		api = get_api_instance()
	return api

@frappe.whitelist()
def is_authenticated():
	try:
		tokens = frappe.get_doc('OAuth Token', frappe.session.user)
		if tokens:
			return True
	except frappe.DoesNotExistError:
		return False

# Testing API

@frappe.whitelist()
def who_am_i():
	api = get_user_api()
	return api.me().name

@frappe.whitelist()
def get_mentions():
	return frappe.get_list('Twitter User', fields=['name', 'screen_name', 'username'])

@frappe.whitelist()
def get_elon():
	api = get_api_instance()
	return api.get_user('elonmusk')._json

# Mentions - https://tweepy.readthedocs.io/en/latest/api.html#API.mentions_timeline
# Get signle status - https://tweepy.readthedocs.io/en/latest/api.html#API.get_status
# Set Status - https://tweepy.readthedocs.io/en/latest/api.html#API.update_status
# Destroy Status - https://tweepy.readthedocs.io/en/latest/api.html#API.destroy_status
# Get User - https://tweepy.readthedocs.io/en/latest/api.html#API.get_user
# Get DMs - https://tweepy.readthedocs.io/en/latest/api.html#API.direct_messages
# Send DM - https://tweepy.readthedocs.io/en/latest/api.html#API.get_direct_message
