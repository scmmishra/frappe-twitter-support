from __future__ import unicode_literals
import frappe
from twit.api import get_user_api

def sync():
	sync_mentions()

def sync_mentions():
	api = get_user_api()
	timeline = api.mentions_timeline()
	for tweet in timeline:
		status = frappe._dict(tweet._json)
		create_user(frappe._dict(status.user))
		try:
			frappe.get_doc(dict(doctype="Twitter Status", id=status.id, in_reply_to_status_id=status.in_reply_to_status_id, posted_by=status.user['id'] ,text=status.text, created_at=tweet.created_at)).insert()
		except frappe.exceptions.DuplicateEntryError:
			continue
		frappe.db.commit()
	status = timeline[0]
	return status._json

def create_user(user):
	try:
		frappe.get_doc('Twitter User', user.id)
	except frappe.DoesNotExistError:
		frappe.get_doc(dict(doctype="Twitter User", user_id=user.id, screen_name=user.screen_name, username=user.name)).insert()
		frappe.db.commit()
