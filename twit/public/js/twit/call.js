frappe.ready(() => {
    frappe.provide('twit');

    twit.call = (method, args) => {
        const method_path = 'twit.api.' + method;
        return new Promise((resolve, reject) => {
            return frappe.call({
                method: method_path,
                args,
            })
            .then(r => resolve(r.message))
            .fail(reject)
        });
    }
});