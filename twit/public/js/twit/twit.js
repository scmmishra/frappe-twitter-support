import Vue from 'vue/dist/vue.js';
import App from './App.vue';
import router from './router';

import FeatherIcon from './components/Statics/FeatherIcon.vue';

// Component registration
Vue.component('FeatherIcon', FeatherIcon);

Vue.config.productionTip = false;

// Vue.use(PortalVue)


frappe.ready(() => {
	frappe.provide('twit')

	twit.store = new Vue({
		methods: {
			isLoggedIn: function() {
				if(frappe.session.user === "Guest") {
					return false
				}
				return true
			}
		}
	});


	twit.app = new Vue({
		router,
		render: h => h(App),
	}).$mount('#app');
})