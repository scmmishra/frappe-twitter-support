import Vue from 'vue/dist/vue.js';
import VueRouter from 'vue-router/dist/vue-router.js'
import Messages from './views/Messages.vue';

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Messages
    },
    {
      path: '/message/',
      name: 'message-home',
      component: Messages
    },
    {
      path: '/message/:id',
      name: 'message',
      component: Messages,
      props: true
    }
  ]
});

export default router;
