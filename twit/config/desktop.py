# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Twitter Support",
			"category": "Modules",
			"label": _("Twitter Support"),
			"color": "#1abc9c",
			"icon": "fa fa-twitter",
			"type": "module",
			"disable_after_onboard": 1,
			"description": "Twtiter Support App.",
			"onboard_present": 0
		}
	]
