from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("API Settings"),
			"items": [
				{
					"type": "doctype",
					"name": "Twitter API Settings"
				},
				{
					"type": "doctype",
					"name": "OAuth Token"
				}
			]
		},
		{
			"label": _("Masters"),
			"items": [
				{
					"type": "doctype",
					"name": "Twitter User"
				},
				{
					"type": "doctype",
					"name": "Twitter Status"
				},
				{
					"type": "doctype",
					"name": "Twitter Chat Thread"
				},
				{
					"type": "doctype",
					"name": "Twitter Direct Message"
				}
			]
		}
	]