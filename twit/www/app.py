from __future__ import unicode_literals
import frappe

no_cache = 1

def get_context(context):
	if frappe.session.user == "Guest" or not has_access():
		frappe.local.flags.redirect_location = '/login#login'
		raise frappe.Redirect

def has_access():
	if frappe.session.user == "Administrator":
		return True
	user = frappe.get_doc("User", frappe.session.user)
	roles = set([role.role for role in user.roles])
	if "Twitter Support User" in roles:
		return True

	return False